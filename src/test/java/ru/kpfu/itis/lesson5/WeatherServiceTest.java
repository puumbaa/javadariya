package ru.kpfu.itis.lesson5;

import org.junit.jupiter.api.*;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class WeatherServiceTest {
    public static void main(String[] args) {
//        Lifecycle.PER_CLASS
//        WeatherServiceTest.beforeAll();
//
//        WeatherServiceTest weatherServiceTest = new WeatherServiceTest();
//        weatherServiceTest.beforeEach();
//        weatherServiceTest.test1();
//        weatherServiceTest.afterEach();
//
//        weatherServiceTest.beforeEach();
//        weatherServiceTest.test2();
//        weatherServiceTest.afterEach();

//        Lifecycle.PER_METHOD (default)
//        WeatherServiceTest weatherServiceTest = new WeatherServiceTest();
//        weatherServiceTest.beforeEach();
//        weatherServiceTest.test1();
//        weatherServiceTest.afterEach();
//
//        WeatherServiceTest weatherServiceTest1  = new WeatherServiceTest();
//        weatherServiceTest.beforeEach();
//        weatherServiceTest.test2();
//        weatherServiceTest.afterEach();

//        WeatherServiceTest.afterAll();
    }

    private WeatherService weatherService;
    @Mock
    private WeatherApiClient apiClient;
    private AutoCloseable autoCloseable;

    private static final String GOOD_WEATHER = "The weather is good";
    private static final String COLD_WEATHER = "The weather is cold";

    @BeforeEach
    void init(){
        autoCloseable = MockitoAnnotations.openMocks(this);
        weatherService = new WeatherService(apiClient);
    }

    @AfterEach
    void tearDown(){
        try {
            if (autoCloseable != null) {
                autoCloseable.close();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void returnsCorrectMessageWhenWeatherPositve() {
        Mockito.when(apiClient.getWeather()).thenReturn(new Weather(5));
        String actual = weatherService.getWeather();
        Mockito.verify(apiClient, Mockito.times(1)).getWeather();
        Assertions.assertEquals(GOOD_WEATHER, actual);
    }

    @Test
    void returnsCorrectWeatherWhenWeatherNegative(){
        Mockito.when(apiClient.getWeather()).thenReturn(new Weather(-1));
        String actual = weatherService.getWeather();
        Assertions.assertEquals(COLD_WEATHER, actual);
    }
}