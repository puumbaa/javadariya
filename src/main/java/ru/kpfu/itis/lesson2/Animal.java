package ru.kpfu.itis.lesson2;

public abstract class Animal {
    protected String country;
    protected String color;

    public Animal(String country, String color) {
        this.country = country;
        this.color = color;
    }

    public abstract void goHunter();
}
