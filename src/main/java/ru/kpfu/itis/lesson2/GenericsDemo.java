package ru.kpfu.itis.lesson2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenericsDemo <T> {

    public static void main(String[] args) {
//        List list = new ArrayList();
//        list.add("123");
//        list.add(12);
//
//        List<Integer> integerList = new ArrayList<>();
//        integerList.add("123");
//
//        int o = (int) list.get(0);
        List<Double> doubles = Arrays.asList(1.4, 1.5, 2.5);
        List<Integer> integers = Arrays.asList(1, 1, 2);
        print(doubles);
        print(integers);



    }


    public static void print(List<? extends Number> list) {
        int sum = 0;
        for (Number number : list) {
           sum += number.intValue();
        }
        System.out.println("sum = " + sum);
    }


    public static <T> void genericMethod(T parameter){
        System.out.println(parameter);
    }


}
