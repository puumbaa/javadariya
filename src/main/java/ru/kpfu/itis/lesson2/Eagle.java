package ru.kpfu.itis.lesson2;

public class Eagle extends Animal {
    public Eagle() {
        super("Америка", "Черный");
    }

    @Override
    public void goHunter() {
        System.out.println("Орел парит над своей жертвой");

    }
}
