package ru.kpfu.itis.lesson2;

public class Tiger extends Animal {

    public Tiger() {
        super("Россия", "Черно-оранжевый");
    }

    @Override
    public void goHunter() {
        String message ="%s тигр из %s крадется к своей добычи...".formatted(color, country);
        System.out.println(message);
    }
}
