package ru.kpfu.itis.lesson2;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Tiger tiger = new Tiger();
        Eagle eagle = new Eagle();
//        Animal[] animals = new Animal[] {tiger, eagle};
        Tiger[] animals = new Tiger[]{tiger};
        goHunter(animals);

        List<Tiger> tigers = List.of(tiger);
        List<Eagle> eagles = List.of(eagle);
        goHunter(tigers);
        goHunter(eagles);

        List<Integer> integerList = new LinkedList<>();

        // code ...
        integerList.add(1);
    }

    public static void iterate(List<Integer> list ){
        for (Integer integer : list) {
            System.out.println(list);
        }
    }

    public static void goHunter(Animal[] animals) {
        for (Animal animal : animals) {
            animal.goHunter();
        }
    }

    // List<? super Animal> - write / read only Object | Контрвариантность
    // List<? extends Animal>  - read / not write | Ковариантность
    // List<Integer> - read / write  | Инвариантность
    public static void goHunter(List<? extends Animal> animals) {
        for (Animal animal : animals) {
            animal.goHunter();
        }
//        animals.add(new Tiger()); error
    }
}
