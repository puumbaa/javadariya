package ru.kpfu.itis.lesson6;

import java.io.*;
import java.util.Arrays;

public class IODemo {
    public static void main(String[] args) throws IOException {
//          (Input/Output)Stream | Reader/Writer

//        byte[] source = new byte[]{1, 2, 3, 4, 5, (byte) 255};
//        InputStream inputStream = new ByteArrayInputStream(source);
//        System.out.println(inputStream.read());
//        System.out.println(inputStream.read());
//        System.out.println(inputStream.read());
//        System.out.println(inputStream.read());
//        System.out.println(inputStream.read());
//        System.out.println(inputStream.read());
//        System.out.println(inputStream.read());

//        InputStream fileInputStream = new FileInputStream("test.txt");
//        int availableBites = fileInputStream.available();
//        byte[] destination = new byte[availableBites];
//        fileInputStream.read(destination);
//        System.out.println(Arrays.toString(destination));


//        try (FileReader reader = new FileReader("test.txt");
//             BufferedReader bufferedReader = new BufferedReader(reader)) {
//
//            StringBuilder result = new StringBuilder();
//            char c = (char) bufferedReader.read();
//            while (c != (char) -1) {
//                result.append(c);
//                c = (char) bufferedReader.read();
//            }
//            System.out.println(result);
//
//        } catch (IOException e) {
//            throw new IllegalArgumentException("Не удалось считать файл test.txt", e);
//        }
//
//
//        try (FileWriter writer = new FileWriter("test.txt", true);
//             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
//            bufferedWriter.write("Как дела?");
//            bufferedWriter.flush();
//            bufferedWriter.write("Нормально");
//            bufferedWriter.write("Ты как ?");
//            bufferedWriter.write("Тоже норм");
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
        Reader reader = new InputStreamReader(new FileInputStream("test.txt"));

        File file = new File("temp/test.txt");
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        file.createNewFile();
//                System.out.println(file.exists());
//        file.createNewFile();
    }
}
