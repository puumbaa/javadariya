package ru.kpfu.itis.lesson7;


import java.io.*;

public class Serializer {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
//
//        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("cats-storage"));
//        objectOutputStream.writeObject(new Cat("Борис", "Черный", 2));

        FileInputStream fileInputStream = new FileInputStream("cats-storage");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);


        Cat cat = (Cat) objectInputStream.readObject();
        System.out.println(cat);
    }
}
