package ru.kpfu.itis.lesson7;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Current thread name: " + Thread.currentThread().getName());
        WorkerThread workerThread = new WorkerThread();
        workerThread.start();
//        workerThread.run();
        workerThread.join(1000); // Main -> WorkerThread
        System.out.println("Thread " + Thread.currentThread().getName() + " end execution");
//  Another way to create a thread instance
//        Thread thread = new Thread(() -> {
//            System.out.println("hello from runnable. Thread name: " + Thread.currentThread().getName());
//        });
//        thread.start();
    }
}
