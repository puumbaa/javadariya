package ru.kpfu.itis.lesson7;

import lombok.AllArgsConstructor;
import lombok.ToString;
import ru.kpfu.itis.lesson2.Animal;

import java.io.Serializable;

public class Cat extends Animal implements Serializable {
    private String name;
    private String color;
    private int age;
    private Person owner;

    public Cat(String country, String color) {
        super(country, color);
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public void goHunter() {

    }
}
