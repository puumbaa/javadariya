package ru.kpfu.itis.lesson7;

public class WorkerThread extends Thread{
    @Override
    public void run() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("hello from " + Thread.currentThread().getName());
    }
}
