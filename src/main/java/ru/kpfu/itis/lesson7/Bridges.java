package ru.kpfu.itis.lesson7;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

public class Bridges {
    public static void main(String[] args) throws IOException {
        Reader inputStreamReader = new InputStreamReader(System.in);
        // ы - 2 [23, 49]
        int c = inputStreamReader.read();
        System.out.println((char) c);
    }
}
