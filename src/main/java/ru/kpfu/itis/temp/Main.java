package ru.kpfu.itis.temp;

import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;

public class Main {
    static final List<Integer> arrayList = new CopyOnWriteArrayList<>();

    public static void main(String[] args) throws InterruptedException {
        int c = 5;
        Function<Integer, Integer> function = (a) ->{
            int b = a + c;
            return b;
        };
        MyThread myThread = new MyThread(function);

        List<Object> objects = Collections.synchronizedList(new ArrayList<>());
        for (int i = 0; i < 10000; i++) {
            new Thread(() -> {
                for (int j = 0; j < 100000; j++) {
                    arrayList.add(j);
                }
            }).start();
        }
        Thread.sleep(5000);
    }

    @AllArgsConstructor
    static class MyThread extends Thread{
        Function<Integer, Integer> function;

        @Override
        public void run() {
            function.apply(4);
        }
    }

}
