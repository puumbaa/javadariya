package ru.kpfu.itis.lesson4;

public class ChildB extends Parent{
    @Override
    public void doSomethingChildSpecific() {
        System.out.println("Child B implementation");
    }
}
