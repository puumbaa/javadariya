package ru.kpfu.itis.lesson4;

public class ChildA extends Parent{
    @Override
    public void doSomethingChildSpecific() {
        System.out.println("Child A implementation");
    }
}
