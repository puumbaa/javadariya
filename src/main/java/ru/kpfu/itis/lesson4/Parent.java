package ru.kpfu.itis.lesson4;

import java.util.List;

public abstract class Parent {

    public abstract void doSomethingChildSpecific();


    // шаблонный метод
    public void doAction() {
        // code ...
        doSomethingChildSpecific();
        // code ...
        // another one abstract method call
    }
}
