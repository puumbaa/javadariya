package ru.kpfu.itis.lesson4;

import java.util.Collection;
import java.util.Iterator;
import java.util.function.BiFunction;
import java.util.function.Function;

public class FunctionsDemo {
    public static void main(String[] args) {
        Function<String, Integer> mapFunction = String::length;
        Function<String, Integer> mapper = FunctionsDemo::customMapper;

        BiFunction<String, String, Integer> biFun = new BiFunction<String, String, Integer>() {
            @Override
            public Integer apply(String x, String y) {
                return x.length() + y.length();
            }
        };
        BiFunction<String, String, Integer> biFunction = (x, y) -> x.length() + y.length();

        Collection<Integer> collection = new Collection<>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<Integer> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(Integer integer) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends Integer> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }
        };
    }

    public static Integer customMapper(String str){
        return 1;
    }
}
