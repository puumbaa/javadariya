package ru.kpfu.itis.lesson3;

import java.util.ArrayList;
import java.util.List;

public class WrappersDemo {

    private int a;
    private Integer b;

    public static void main(String[] args) {

        WrappersDemo wrappersDemo = new WrappersDemo();
//        System.out.println(wrappersDemo.a);
//        System.out.println(wrappersDemo.b);

        String s1 = "abc";
        String s2 = "abc";
        System.out.println(s1 == s2);
        Integer i1 = 1200;
        Integer i2 = 1200;
        System.out.println(i1 == i2);

//        Integer integer = 5; // Boxing - упаковка | при компиляция => new Integer(5)
//        Integer int2 = 5;
//        int x = 5;
//        List<Integer> list = new ArrayList<>();
//
//        long l = 5;
//        Long longNumber = 5L;
//        Float floatNumber = 5.0F;
//

    }

    public static int test(Integer integer) {
        return integer; // при компиляции => integer.intValue();
    }
}
