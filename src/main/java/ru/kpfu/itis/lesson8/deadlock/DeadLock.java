package ru.kpfu.itis.lesson8.deadlock;

public class DeadLock {
    public static void main(String[] args) {
        Resource resource1 = new Resource(1);
        Resource resource2 = new Resource(2);

        new MyThread(resource1, resource2).start();
        new MyThread(resource2, resource1).start();
    }

    static class MyThread extends Thread {
        private final Resource resource1;
        private final Resource resource2;


        public MyThread(Resource resource1, Resource resource2) {
            this.resource1 = resource1;
            this.resource2 = resource2;
        }

        @Override
        public void run() {
            String name = Thread.currentThread().getName();
            System.out.println(name + " start working...");
            synchronized (resource1){
                System.out.println(name + " get resource 1 lock"); // t0, t1
                synchronized (resource2) {
                    System.out.println(name + " get resource 2 lock");
                }
            }
            System.out.println(name + " finished");
        }
    }

}
