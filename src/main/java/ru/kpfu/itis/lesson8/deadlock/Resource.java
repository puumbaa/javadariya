package ru.kpfu.itis.lesson8.deadlock;

public class Resource {
    private int value;

    public Resource(int value) {
        this.value = value;
    }

    public void setValue(int value) {
        synchronized (this) {
            this.value = value;
        }
    }

//    public synchronized void setValue(int value) {
//       this.value = value;
//    }


}
