package ru.kpfu.itis.lesson8.problem;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Resource resource = new Resource();
        resource.value = 0;

        MyThread myThread1 = new MyThread(resource);
        MyThread myThread2 = new MyThread(resource);

        myThread1.start();
//        myThread1.join();
        // thread 1: value = 1
        myThread2.start();
        // thread 2 value = 2
    }

    static class Resource{
        private volatile int value;

        public synchronized int getValue(){
            return value;
        }

        public synchronized void setValue(int value){
            this.value = value;
        }
    }

    static class MyThread extends Thread{
        private final Resource resource;

        public MyThread(Resource resource) {
            this.resource = resource;
        }

        @Override
        public void run() {
            int value = resource.getValue();
            value++;
            resource.setValue(value);

            System.out.printf("Value = %d from thread: %s", resource.value, Thread.currentThread().getName());
            System.out.println();
        }
    }
}
