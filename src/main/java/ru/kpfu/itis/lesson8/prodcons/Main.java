package ru.kpfu.itis.lesson8.prodcons;


public class Main {
    public static void main(String[] args) throws InterruptedException {
        Resource resource = new Resource(false);
        Producer producer = new Producer(resource);
        producer.start();
        Consumer consumer = new Consumer(resource);
        consumer.start();

        Thread.sleep(3000);
        producer.setStop(true);
        consumer.setStop(true);

        producer.join();
        consumer.join();

        System.out.println("Producer & consumer finished!");
    }
}
