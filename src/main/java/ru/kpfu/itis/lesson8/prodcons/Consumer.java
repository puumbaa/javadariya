package ru.kpfu.itis.lesson8.prodcons;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Consumer extends Thread{
    private final Resource resource;
    private volatile boolean stop = false;
    public Consumer(Resource resource) {
        this.resource = resource;
    }

    @Override
    public void run() {
        while (!stop) {
            try {
                consume();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public  void consume() throws InterruptedException {
        synchronized (resource) {
            while (resource.isConsumerReadValue()) {
                System.out.println("consumer wait producer");
                resource.wait();
            }
            System.out.println("consumer read value");
            resource.setConsumerReadValue(true);
            resource.notify();
        }
    }
}
