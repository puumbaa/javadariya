package ru.kpfu.itis.lesson8.prodcons;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Producer extends Thread{
    private final Resource resource;
    private volatile boolean stop = false;
    // false => consumer прочитал значение
    // true => producer положил значение, но consumer его еще не прочитал

    public Producer(Resource value) {
        this.resource = value;
    }

    @Override
    public void run() {
        try {
            while (!stop){
                produce();
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public void produce() throws InterruptedException {
        synchronized (resource) {
            while (!resource.isConsumerReadValue()) {
                System.out.println("producer wait consumer");
                resource.wait();
            }
            System.out.println("producer write new value");
            resource.setConsumerReadValue(false); // произвел новое значение
            System.out.println("producer notify consumer that there is a new value to read");
            resource.notify();
        }
    }
}
