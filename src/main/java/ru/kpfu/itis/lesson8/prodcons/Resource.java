package ru.kpfu.itis.lesson8.prodcons;

public class Resource {
    private volatile boolean isConsumerReadValue;

    public Resource(boolean isConsumerReadValue) {
        this.isConsumerReadValue = isConsumerReadValue;
    }

    public boolean isConsumerReadValue() {
        return isConsumerReadValue;
    }

    public void setConsumerReadValue(boolean consumerReadValue) {
        this.isConsumerReadValue = consumerReadValue;
    }
}
