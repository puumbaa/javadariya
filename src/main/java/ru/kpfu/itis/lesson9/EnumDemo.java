package ru.kpfu.itis.lesson9;

public enum EnumDemo {
    MALE("Мужской пол"), FEMALE("Женский пол");

    public String getTranslate() {
        return translate;
    }

    public void setTranslate(String translate) {
        this.translate = translate;
    }

    public void someMethod(){
        System.out.println("blabla");
    }

    private String translate;

    EnumDemo(String translate){
        this.translate = translate;
    }

}
