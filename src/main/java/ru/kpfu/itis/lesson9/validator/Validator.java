package ru.kpfu.itis.lesson9.validator;

import ru.kpfu.itis.lesson9.users.User;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.Arrays;

public class Validator {

    public static void main(String[] args) throws Exception {
        User user = new User(1L, "Alex", "Sanchez", LocalDate.of(2001, 2, 1));
        User baby = new User(1L, "Alex", "Junior", LocalDate.of(2009, 2, 1));
        Validator validator = new Validator();
        System.out.println(validator.validateUser(user) ? "Валидно" : "Мало лет");
        validator.validateUser(baby);
    }

//    static void validate(User user) throws Exception{
//        Class<? extends User> userClass = user.getClass();
//        System.out.println(Arrays.toString(userClass.getDeclaredFields()));
//
//        Field nameField = userClass.getDeclaredField("name");
//        nameField.setAccessible(true); // я могу получать доступ к приватным полям
//        String nameValue = (String) nameField.get(user);
//        System.out.println(nameField);
//        System.out.println(nameValue);
//
//        Method userMethod = userClass.getMethod("blabla", String.class);
//        userMethod.invoke(user, "hello world");
////        System.out.println(Arrays.toString(userClass.getDeclaredField("birthdate").getAnnotations()));
//
//    }

    /**
     *  Метод для валидации юзера => надо проверить поля объекта user на наличие аннотаций @ValidateDate
     *  Если такая аннотация стоит над полем, то мы проверяем поле на валидность,
     *  если поле валидно мы возвращем true, если не валидно - бросаем исключение
     **/
    boolean validateUser(User user) throws IllegalAccessException {
        Class<User> userClass = User.class;
        Field[] userFields = userClass.getDeclaredFields();

        for (Field field : userFields) {
            field.setAccessible(true);
            ValidateDate annotation = field.getAnnotation(ValidateDate.class);
            if (annotation != null) {
                LocalDate date = (LocalDate) field.get(user);
                int age = annotation.age();
                if (!date.isBefore(LocalDate.now().minusYears(age))){
                    int years = LocalDate.now().minusYears(date.getYear()).getYear();
                    throw new IllegalStateException(
                            "Пользователю мало лет: пользователю " + years + " лет." +
                                    " Минимальный допустимый возраст - " + age);
                }
            }
        }
        return true;
    }
}
