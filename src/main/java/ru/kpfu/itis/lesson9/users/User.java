package ru.kpfu.itis.lesson9.users;


import lombok.AllArgsConstructor;
import lombok.Data;
import ru.kpfu.itis.lesson9.validator.ValidateDate;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class User {
    private Long id;
    private String name;
    private String surname;
    @ValidateDate(age = 10)
    private LocalDate birthdate;

    public void blabla(String x) {
        System.out.println(x);
    }
}
