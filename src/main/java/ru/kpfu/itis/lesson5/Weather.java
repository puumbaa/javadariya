package ru.kpfu.itis.lesson5;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Weather {
    private double temp;

}
