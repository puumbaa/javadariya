package ru.kpfu.itis.lesson5;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class WeatherService {
    private final WeatherApiClient client;

    public String getWeather(){
        if (client.getWeather().getTemp() > 0) {
            return "The weather is good";
        }else {
            return "The weather is cold";
        }
    }
}
