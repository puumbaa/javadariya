package ru.kpfu.itis.lesson5;

import java.util.Random;

public class WeatherApiClient {

    Weather getWeather(){
        System.out.println("sent http request to get weather info...");
        return new Weather(new Random().nextDouble());
    }
}
