package ru.kpfu.itis.practise;

import ru.kpfu.itis.practise.pr1.Person;
import ru.kpfu.itis.practise.pr1.PersonParser;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        PersonParser personParser = new PersonParser();
        List<Person> persons = personParser.parse("files/persons.csv", ",");
        System.out.println(persons);
        System.out.println(getStatistic(persons));
    }

    public static Map<String, Long> getStatistic(List<Person> personList) {
        return personList.stream()
                .collect(Collectors.groupingBy(
                        Person::getCountry,
                        Collectors.counting()
                ));
    }
}
