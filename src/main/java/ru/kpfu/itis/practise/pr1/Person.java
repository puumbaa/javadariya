package ru.kpfu.itis.practise.pr1;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Person {
    String name;
    String country;
    int birthYear;
    int deathYear;
}
