package ru.kpfu.itis.practise.pr1;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Event {
    String name;
    String personName;
    int startYear;
    int endYear;
}
