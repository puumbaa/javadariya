package ru.kpfu.itis.practise.pr1;

import ru.kpfu.itis.practise.pr3.ObjectParser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class PersonParser implements ObjectParser<Person> {


    @Override
    public Person map(String[] strings) {
        return new Person(
                strings[0].trim(),
                strings[1].trim(),
                Integer.parseInt(strings[2].trim()),
                Integer.parseInt(strings[3].trim()));
    }
}
