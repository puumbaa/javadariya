package ru.kpfu.itis.practise.pr1;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class Runner {
    public static void main(String[] args) {
        EventParser eventParser = new EventParser();
        PersonParser personParser = new PersonParser();
        List<Person> persons = personParser.parse("files/persons.csv", ",");
        List<Event> events = eventParser.parse("files/events.csv", ",");


        Map<String, Long> collect = persons.stream()
                .collect(Collectors.groupingBy(Person::getCountry, Collectors.counting()));

        System.out.println(collect);

        System.out.println(task3(events, persons, "Америка"));
    }

    public static long task3(List<Event> events, List<Person> persons, String country) {
        Map<String, List<Person>> personsByName = persons.stream()
                .collect(Collectors.groupingBy(Person::getName));

        return events.stream()
                .filter(event -> isGoodEvent(country, personsByName, event))
                .count();
    }

    private static boolean isGoodEvent(String country, Map<String, List<Person>> personsByName, Event event) {
        return Optional.ofNullable(personsByName.get(event.getPersonName()))
                .map(people -> people.stream().anyMatch(person -> person.getCountry().equals(country) &&
                                event.startYear > (person.birthYear + person.deathYear) / 2))
                .orElse(false);

//        if (people != null) {
//            return people.stream().anyMatch(person ->
//                    person.getCountry().equals(country) &&
//                            event.startYear > (person.birthYear + person.deathYear) / 2);
//        } else {
//            return false;
//        }
    }
}
