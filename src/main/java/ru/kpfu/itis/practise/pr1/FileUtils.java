package ru.kpfu.itis.practise.pr1;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    public static void main(String[] args) {
        copyFile("files/numbers", "output", 100);
    }

    public static void copyFile(String filePath, String outputDir, int numberOfThreads) {
        validateInputArgs(filePath, numberOfThreads);
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line = reader.readLine();
            List<Integer> fileContent = new ArrayList<>();
            while (line != null) {
                String[] split = line.split(",");
                for (String s : split) {
                    int number = Integer.parseInt(s);
                    fileContent.add(number);
                }
                line = reader.readLine();
            }
            createFileCopies(fileContent, numberOfThreads, outputDir);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void createFileCopies(List<Integer> fileContent, int numberOfThreads, String outputDir) {
        for (int i = 0; i < numberOfThreads; i++) {
            File file = new File(outputDir + Path.of(outputDir).getFileSystem().getSeparator() + "out" + i);
            new Thread(() -> {
                try (DataOutputStream dataOutputStream = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {
                    fileContent.forEach(number -> {
                        try {
                            dataOutputStream.writeInt(number);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    });
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }).start();
        }
    }

    private static void validateInputArgs(String filePath, int numberOfThreads) {
        // todo
    }
}
