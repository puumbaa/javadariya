package ru.kpfu.itis.practise.pr1;

import ru.kpfu.itis.practise.pr3.ObjectParser;

import java.util.List;

public class EventParser implements ObjectParser<Event> {

    @Override
    public Event map(String[] strings) {
        return new Event(
                strings[0],
                strings[1],
                Integer.parseInt(strings[2]),
                Integer.parseInt(strings[3])
        );
    }
}
