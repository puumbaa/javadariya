package ru.kpfu.itis.practise.pr3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface ObjectParser<T> {

    T map(String[] strings); // public => protected

    default List<T> parse(String fileName, String delimiter){
        try(BufferedReader reader = new BufferedReader(new FileReader(fileName))){
            List<T> result = new ArrayList<>();
            String line = reader.readLine();
            while (line != null) {
                String[] split = line.split(delimiter); // events = [Война, Петр1, 1023, 1025] | persons = [Петя, Россия, 1990, 2015]
                result.add(map(split));
                line = reader.readLine();
            }
            return result;
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot read file", e);
        }
    }
}
