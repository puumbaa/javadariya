package ru.kpfu.itis.practise.pr3;

public class MessageParser implements ObjectParser<Message> {
    @Override
    public Message map(String[] strings) {
        return new Message(
                Long.parseLong(strings[0]),
                strings[1],
                Long.parseLong(strings[2])
        );
    }
}
