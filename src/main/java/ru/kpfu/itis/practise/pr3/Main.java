package ru.kpfu.itis.practise.pr3;

import netscape.javascript.JSObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        UserParser userParser = new UserParser();
        MessageParser messageParser = new MessageParser();
        LikeParser likeParser = new LikeParser();
        List<User> users = userParser.parse("files/persons.csv", ",");
        List<Message> messages = messageParser.parse("files/messages.tsv", ",");
        List<Like> likes = likeParser.parse("files/likes.tsv", ",");
        task1(users, messages, likes);
    }

    public static Map<User, Long> task1(List<User> users, List<Message> messages, List<Like> likes) {
        Map<Long, Long> likesCountByMessageId = likes.stream()
                .collect(Collectors.groupingBy(Like::getMessageId,
                        Collectors.counting()));

        Map<Long, List<Message>> messagesByAuthorId = messages.stream()
                .collect(Collectors.groupingBy(Message::getAuthorId));

        return users.stream()
                .filter(user -> user.getEmail().endsWith("gmail.com"))
                .collect(Collectors.toMap(
                        user -> user,
                        user -> {
                            List<Message> userMessages = messagesByAuthorId.get(user.getId());
                            return userMessages.stream()
                                    .map(message -> likesCountByMessageId.get(message.getId()))
                                    .reduce(Long::sum)
                                    .orElse(0L);
                        }
                ));
    }

    public static List<User> task2(List<User> users, List<Message> messages, List<Like> likes) {

        Map<Long, List<Like>> likesByUserId = new HashMap<>();
        for (Like like : likes) {
            List<Like> likeByUserId = likesByUserId.get(like.getUserId());
            if (likeByUserId == null) {
                likeByUserId = new ArrayList<>();
            }
            likeByUserId.add(like);
            likesByUserId.put(like.getUserId(), likeByUserId);
        }

        Map<Long, User> usersById = new HashMap<>();
        for (User user : users) {
            usersById.put(user.getId(), user);
        }

        Map<Long, List<Message>> messagesById = new HashMap<>();
        for (Message message : messages) {
            List<Message> messageList = messagesById.get(message.getId());
            if (messageList == null) {
                messageList = new ArrayList<>();
            }
            messageList.add(message);
            messagesById.put(message.getId(), messageList);
        }


        List<User> result = new ArrayList<>();
        for (User user : users) {
            List<Like> likeList = likesByUserId.get(user.getId());
            boolean isGoodUser = true;
            for (Like like : likeList) {
                List<Message> messageList = messagesById.get(like.getMessageId());
                for (Message message : messageList) {
                    Long authorId = message.getAuthorId();
                    User author = usersById.get(authorId);
                    if (!author.getCity().equals(user.getCity())) {
                        isGoodUser = false;
                        break;
                    }
                }
                if (!isGoodUser) {
                    break;
                }
            }
            if (isGoodUser) {
               result.add(user);
            }
        }
        return result;
    }
}
