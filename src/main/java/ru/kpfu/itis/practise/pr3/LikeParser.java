package ru.kpfu.itis.practise.pr3;

public class LikeParser implements ObjectParser<Like> {
    @Override
    public Like map(String[] strings) {
        return new Like(
                Long.parseLong(strings[0]),
                Long.parseLong(strings[1])
        );
    }
}
