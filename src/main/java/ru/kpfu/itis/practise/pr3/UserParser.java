package ru.kpfu.itis.practise.pr3;



public class UserParser implements ObjectParser<User> {

    @Override
    public User map(String[] strings) {
        return new User(
                Long.parseLong(strings[0]),
                strings[1],
                strings[2],
                strings[3],
                Integer.parseInt(strings[4])
        );
    }
}
