package ru.kpfu.itis.practise.pr3;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Like {
    private Long userId;
    private Long messageId;
    // Map<messageId, count>
}
