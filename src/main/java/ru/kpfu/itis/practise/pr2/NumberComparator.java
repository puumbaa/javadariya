package ru.kpfu.itis.practise.pr2;

import java.util.Comparator;

public class NumberComparator implements Comparator<Integer> {
    @Override
    public int compare(Integer a, Integer b) {
        return a.toString().replace("-", "").length()
                - b.toString().replace("-", "").length();
    }
}
