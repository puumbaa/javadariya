package ru.kpfu.itis.lesson10;

import ru.kpfu.itis.lesson9.users.User;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException {
        Class<?> user = Class.forName("ru.kpfu.itis.lesson9.users.User");
        System.out.println(user);
//        InstanceCreator creator = new InstanceCreator();
//        System.out.println(creator.createInstance(
//                User.class,
//                new Object[]{1L, "A", "B", LocalDate.now()},
//                Long.class, String.class, String.class, LocalDate.class));
    }

//    public static int sum(int a, int b){
//        return a + b;
//    }
//
//    public static int sum(int a, int b, int c){
//        return a + b + c;
//    }
    public static int sum(int... num) {
        int sum = 0;
        for (int i : num) {
            sum += i;
        }
        return sum;
    }

}
