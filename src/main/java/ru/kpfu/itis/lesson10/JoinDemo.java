package ru.kpfu.itis.lesson10;

public class JoinDemo {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(() -> {
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println(Thread.currentThread().getName() + " finished");
        });
        thread.start();

        thread.join();
        System.out.println(Thread.currentThread().getName() + " finished");
    }
}
