package ru.kpfu.itis.lesson10;

import ru.kpfu.itis.lesson9.users.User;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class InstanceCreator {
    // createInstance(Class<String>, Class<Integer>)
    public Object createInstance(Class<?> clazz, Object[] parameterValues, Class<?>... parameterTypes){
        Constructor<?> constructor = null;
        try {
            Method method = clazz.getMethod("", String.class);
            constructor = clazz.getDeclaredConstructor(parameterTypes);
            // [Class<String>, Class<Integer>]  => User(String a, Integer b) {...}
            constructor.setAccessible(true);
            return constructor.newInstance(parameterValues); // new User("", "")
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (constructor != null) {
                constructor.setAccessible(false);
            }
        }
    }
}
